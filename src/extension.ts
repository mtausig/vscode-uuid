// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
require('uuid');
import { v4 as uuidv4 } from 'uuid';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "uuid" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('uuid.insertUuid4', () => {
		// The code you place here will be executed every time your command is executed

		vscode.window.activeTextEditor?.edit(edit => {
			// Check if we are in an editor
			if(vscode.window.activeTextEditor !== undefined){
				const uuid: string = uuidv4();
				const currentSelection: vscode.Selection = vscode.window.activeTextEditor.selection;
				const currentPosition: vscode.Position = currentSelection.active;
				if(currentSelection.isEmpty) {
					// Add the UUID at the current position
					edit.insert(currentPosition, uuid);
				} else {
					// Replace the current selection with the UUID
					edit.replace(currentSelection, uuid);
				};
			}		
		});
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
