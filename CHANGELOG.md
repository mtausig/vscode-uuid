# Change Log

All notable changes to the "uuid" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [1.1.0] - 2021-04-09

- Publish packages to Gitlab's package repository
- Add licence information

## [1.0.0] - 2021-04-02

- Initial release