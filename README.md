# uuid README

This is an extension for VSCodium/Visual Studio Code which allows you to insert a UUID into your text.

## Features

This extension exposes a single command called `Insert UUIDv4` which does exactly what it says.
Simply use the command palette to execute it.

## Requirements

None

## Extension Settings

None

## Known Issues

None


